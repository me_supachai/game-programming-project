﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;

public class ObjectInteraction : MonoBehaviour {

	public GameObject barrier;
	public Text pickUpText;
	public Text pressText;
	public Text openDoorText;
	private Renderer gun;
	private Transform raycastSource;
	private bool isInInteractionRadius = false;
	private MovableObject holdingObject;
	private FirstPersonController fpc;
	private bool isholdingObject = false;
	private bool canThrowObject = false;
	private float holdTimer = 0f;


	// Update is called once per frame
	void Start () {
		pickUpText.enabled = false;
		pressText.enabled = false;
		openDoorText.enabled = false;
		raycastSource = transform.Find ("Raycast Start Position");
		gun = transform.parent.Find("Gun").Find ("ShooterFPSWeapon").gameObject.GetComponent<Renderer>();
		fpc = GameObject.FindGameObjectWithTag ("Player").GetComponent<FirstPersonController> ();
		fpc.GetMouseLook ().MaximumX = 80;
	}

	void Update() {
		if (!isholdingObject && !barrier.activeSelf && !fpc.IsJumping ()) {
			RaycastHit hit;
			Vector3 direction = (transform.position - raycastSource.position).normalized;
			if (Physics.Raycast (raycastSource.position, direction, out hit, 1.8f)) {
				if (hit.collider.tag == "Movable Object" && fpc.GetMouseLook ().CurrentX < 48f && !fpc.IsCrouching ()) {
					pickUpText.enabled = true;
					pressText.enabled = false;
					openDoorText.enabled = false;
					if (Input.GetKeyDown (KeyCode.F)) {
						pickUpText.enabled = false;
						isholdingObject = true;
						gun.enabled = false;
						holdingObject = hit.collider.gameObject.GetComponent<MovableObject> ();
						fpc.SetHoldingObject (true);
						RestrictMovement ();
						holdingObject.MoveToPlayer (transform.Find ("Object Holding Position").position);
					}
				} else if (hit.collider.tag == "Small Movable Object") {
					pickUpText.enabled = true;
					pressText.enabled = false;
					openDoorText.enabled = false;
					if (Input.GetKeyDown (KeyCode.F)) {
						pickUpText.enabled = false;
						isholdingObject = true;
						gun.enabled = false;
						holdingObject = hit.collider.gameObject.GetComponent<MovableObject> ();
						holdingObject.MoveToPlayer (transform.position);
					}
				} else if (hit.collider.tag == "Light Switch") {
					pickUpText.enabled = false;
					pressText.enabled = true;
					openDoorText.enabled = false;
					if (Input.GetKeyDown (KeyCode.F)) {
						hit.collider.gameObject.GetComponent<Switch>().ToggleLight ();
					}
				} else if (hit.collider.tag == "Door" || hit.collider.tag == "Door RK") {
					pickUpText.enabled = false;
					pressText.enabled = false;
					openDoorText.enabled = true;
					if (hit.collider.tag == "Door") {
						if (hit.collider.gameObject.GetComponent<SlideDoor> ().IsOpen ()) {
							openDoorText.text = "Press 'F' to close";
						} else {
							openDoorText.text = "Press 'F' to open";
						}
					} else {
						if (hit.collider.gameObject.GetComponent<SlideDoor1> ().IsOpen ()) {
							openDoorText.text = "Press 'F' to close";
						} else {
							openDoorText.text = "Press 'F' to open";
						}
					}
					if (Input.GetKeyDown (KeyCode.F)) {
						if (hit.collider.tag == "Door") {
							hit.collider.gameObject.GetComponent<SlideDoor> ().ToggleDoor ();
						} else {
							hit.collider.gameObject.GetComponent<SlideDoor1> ().ToggleDoor ();
						}
					}
				} else if (hit.collider.tag == "Key") {
					if (!hit.collider.gameObject.GetComponent<Key> ().IsTaken ()) {
						pickUpText.enabled = true;
						pressText.enabled = false;
						openDoorText.enabled = false;
						if (Input.GetKeyDown (KeyCode.F)) {
							pickUpText.enabled = false;
							hit.collider.gameObject.GetComponent<Key> ().PickUp ();
						}
					}
				} else {
					pickUpText.enabled = false;
					pressText.enabled = false;
					openDoorText.enabled = false;
				}
			} else {
				pickUpText.enabled = false;
				pressText.enabled = false;
				openDoorText.enabled = false;
			}
		} else if (isholdingObject) {
			if (Input.GetKeyUp (KeyCode.F) && holdingObject.IsBeingHeld () && canThrowObject) {
				holdTimer = 0f;
				canThrowObject = false;
				isholdingObject = false;
				gun.enabled = true;
				fpc.SetHoldingObject (false);
				ResetMovement ();
				holdingObject.Release ();
			} else if (Input.GetKeyUp (KeyCode.F)) {
				canThrowObject = true;
			} else if (Input.GetKey (KeyCode.F) && holdingObject.IsBeingHeld () && canThrowObject) {
				holdTimer += Time.deltaTime;
				if (holdTimer > 0.5f) {
					holdTimer = 0f;
					canThrowObject = false;
					isholdingObject = false;
					gun.enabled = true;
					fpc.SetHoldingObject (false);
					ResetMovement ();
					holdingObject.Throw ();
				}
			} 
		} else {
			pickUpText.enabled = false;
			pressText.enabled = false;
			openDoorText.enabled = false;
		}
	}

	public void SetIsInInteractionRadius(bool isInInteractionRadius) {
		this.isInInteractionRadius = isInInteractionRadius;
	}

	private void RestrictMovement() {
		fpc.SetWalkSpeed (2f);
		fpc.SetRunSpeed (2f);
		fpc.GetMouseLook ().XSensitivity = 0.3f;
		fpc.GetMouseLook ().YSensitivity = 0.3f;
		fpc.GetMouseLook ().MinimumX = -20;
		fpc.GetMouseLook ().MaximumX = 48;
	}

	private void ResetMovement() {
		fpc.SetWalkSpeed (5f);
		fpc.SetRunSpeed (10f);
		fpc.GetMouseLook ().XSensitivity = 2f;
		fpc.GetMouseLook ().YSensitivity = 2f;
		fpc.GetMouseLook ().MinimumX = -90;
		fpc.GetMouseLook ().MaximumX = 80;
	}
}
