﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine;
using UnityEngine.AI;

public class MainMenuAI : MonoBehaviour {

	public float walkSpeed;
	public float sprintSpeed;
	public float distractTime;
	private float distractTimer;

	private NavMeshAgent agent ;

	public Transform[] points;
	private int desPoint;

	private bool setDestination = true;
	private bool goOffRoute = false;
	private Vector3 intendedDestination;

	// Use this for initialization
	void Awake () {
		agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
		agent.autoBraking = true;
		agent.updateRotation = true;
		agent.speed = walkSpeed;
		desPoint = 0;
		transform.position = points[desPoint].position;
		intendedDestination = points[desPoint].position;
	}

	// Update is called once per frame
	void Update () {
		if (setDestination) {
			setDestination = false;
			agent.destination = intendedDestination;
			return;
		} else if (agent.remainingDistance < 0.2f) {
			if (!goOffRoute) {
				setDestination = true;
				desPoint = (desPoint + 1) % points.Length;
			} else {
				distractTimer += Time.deltaTime;
				if (distractTimer >= distractTime) {
					goOffRoute = false;
					setDestination = true;
				}
			}
			intendedDestination = points[desPoint].position;
		}
	}

	public void GoOffRoute(Vector3 destination){
		distractTimer = 0f;
		setDestination = true;
		goOffRoute = true;
		intendedDestination = destination;
	}	
}
