﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideDoor : MonoBehaviour {
	private GameObject door;
	private UIManager uiManager;
	public bool endStage;
	public bool turn90;
	public float speed;
	public bool enter;
	public bool open;
	public float OpenX;
	public float CloseX;
	public float OpenY;
	public float CloseY;
	public float OpenZ;
	public float CloseZ;
	public float targetZ;
	public float smoothTime = 1.0f;
	public float newPosition;
	public Vector3 target;
	public Vector3 closePosition;
	public Vector3 openPosition;


	public AudioClip slideSound;
	private AudioSource source;

	void Awake() {
		source = GetComponent<AudioSource>();
	}

	// Use this for initialization
	void Start () {
		door = transform.Find ("Body").gameObject;
		uiManager = GameObject.Find ("UI Manager").GetComponent<UIManager> ();
		CloseX = door.transform.position.x;
		CloseY = door.transform.position.y;
		CloseZ = door.transform.position.z;
		if (turn90) {
			OpenX = CloseX + 5;
			OpenY = CloseY;
			OpenZ = CloseZ;
		} else {
			OpenX = CloseX;
			OpenY = CloseY;
			OpenZ = CloseZ+5;
		}
		Vector3 closePosition = new Vector3 (CloseX, CloseY, CloseZ);
		Vector3 openPosition = new Vector3 (OpenX, OpenY, OpenZ);
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.timeScale == 0f) {
			return;
		}
		if (open) {
			


			if (turn90) {
				if (door.transform.position.x < OpenX)
					door.transform.position = new Vector3 (door.transform.position.x + 0.1f, door.transform.position.y, door.transform.position.z);
			} else {
				if (door.transform.position.z < OpenZ)
					door.transform.position = new Vector3 (door.transform.position.x, door.transform.position.y, door.transform.position.z + 0.1f);
			}
		} else {
			if (turn90) {
				if (door.transform.position.x > CloseX)
					door.transform.position = new Vector3 (door.transform.position.x - 0.1f, door.transform.position.y, door.transform.position.z);
			} else {
				if (door.transform.position.z > CloseZ)
					door.transform.position = new Vector3 (door.transform.position.x , door.transform.position.y, door.transform.position.z- 0.1f);
			}
		}
	}

	public void ToggleDoor() {

		source.Stop ();
		source.PlayOneShot(slideSound,1);
		open = !open;
		if (open && endStage) {
			uiManager.Clear ();
		}
	}

	public bool IsOpen() {
		return open;
	}
}
