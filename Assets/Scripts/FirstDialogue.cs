﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstDialogue : MonoBehaviour {

	public int startLine;
	public int endLine;

	public TextAsset TextFile;

	public DialogueManager theTextBox;


	// Use this for initialization
	void Start () {
		theTextBox = FindObjectOfType<DialogueManager> ();
		theTextBox.ReloadDialogue (TextFile);
		theTextBox.currentLine = startLine;
		theTextBox.endAtLine = endLine;
		theTextBox.EnableTextBox ();

		Destroy (gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
