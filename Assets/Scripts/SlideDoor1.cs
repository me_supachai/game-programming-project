﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideDoor1 : MonoBehaviour {
	private GameObject door;
	private UIManager uiManager;
	public Key key;
	public bool endStage;
	public float r;
	public float g;
	public float b;
	public bool turn90;
	public float speed;
	public bool enter;
	public bool open;
	public bool invert = false;
	public float OpenX;
	public float CloseX;
	public float OpenY;
	public float CloseY;
	public float OpenZ;
	public float CloseZ;
	public float targetZ;
	public float smoothTime = 1.0f;
	public float newPosition;
	public Vector3 target;
	public Vector3 closePosition;
	public Vector3 openPosition;

	public AudioClip slideSound;
	private AudioSource source;

	void Awake() {
		source = GetComponent<AudioSource>();
	}

	// Use this for initialization
	void Start () {
		door = transform.Find ("Body").gameObject;
		uiManager = GameObject.Find ("UI Manager").GetComponent<UIManager> ();
		CloseX = door.transform.position.x;
		CloseY = door.transform.position.y;
		CloseZ = door.transform.position.z;
		if (turn90) {
			OpenY = CloseY;
			OpenZ = CloseZ;
			if (!invert) {
				OpenX = CloseX + 5;
			} else {
				OpenX = CloseX -5;
			}
		} else {
			OpenX = CloseX;
			OpenY = CloseY;
			if (!invert) {
				OpenZ = CloseZ + 5;
			} else {
				OpenZ = CloseZ - 5;
			}
		}
		Vector3 closePosition = new Vector3 (CloseX, CloseY, CloseZ);
		Vector3 openPosition = new Vector3 (OpenX, OpenY, OpenZ);
		door.GetComponent<Renderer> ().material.color = new Color (r, g, b);
		key.GetComponent<Renderer> ().material.color = new Color (r, g, b);
	}

	// Update is called once per frame
	void Update () {
		if (Time.timeScale == 0f) {
			return;
		}
		if (open) {

			if (turn90) {
				if (!invert) {
					if (door.transform.position.x < OpenX)
						door.transform.position = new Vector3 (door.transform.position.x + 0.1f, door.transform.position.y, door.transform.position.z);
				} else {
					if (door.transform.position.x > OpenX)
						door.transform.position = new Vector3 (door.transform.position.x - 0.1f, door.transform.position.y, door.transform.position.z);
				}
			} else {
				if (!invert) {
					if (door.transform.position.z < OpenZ)
						door.transform.position = new Vector3 (door.transform.position.x, door.transform.position.y, door.transform.position.z + 0.1f);
				} else {
					if (door.transform.position.z > OpenZ)
						door.transform.position = new Vector3 (door.transform.position.x, door.transform.position.y, door.transform.position.z - 0.1f);
				}
			}
		} else {
			if (turn90) {
				if (!invert) {
					if (door.transform.position.x > CloseX)
						door.transform.position = new Vector3 (door.transform.position.x - 0.1f, door.transform.position.y, door.transform.position.z);
				} else {
					if (door.transform.position.x < CloseX)
						door.transform.position = new Vector3 (door.transform.position.x + 0.1f, door.transform.position.y, door.transform.position.z);
				}
			} else {
				if (!invert) {
					if (door.transform.position.z > CloseZ)
						door.transform.position = new Vector3 (door.transform.position.x, door.transform.position.y, door.transform.position.z - 0.1f);
				} else {
					if (door.transform.position.z < CloseZ)
						door.transform.position = new Vector3 (door.transform.position.x, door.transform.position.y, door.transform.position.z + 0.1f);
				}
			}
		}
	}

	public void ToggleDoor() {
		if (key.IsTaken ()) {
			source.Stop ();
			source.PlayOneShot(slideSound,1);
			open = !open;
		}
		if (open && endStage) {
			uiManager.Clear ();
		}
	}

	public bool IsOpen() {
		return open;
	}
}
