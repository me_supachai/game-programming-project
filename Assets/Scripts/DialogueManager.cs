﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using System.Text.RegularExpressions;
using System.Linq;
using System;

public class DialogueManager : MonoBehaviour {

	public GameObject backGround;
	public GameObject textBox;
	public Text nameText;
	public Text dialogueText;

	public TextAsset textFile;
	public string[] textLines;

	string[] words;

	public int currentLine;
	public int endAtLine;

	public bool isActive;
	public bool stopMovement;

	public FirstPersonController player;
	public ActivateBarrier barrier;

	private bool isTyping = false;
	private bool cancelTyping = true;

	public float typeSpeed = 0.01f;

	// Use this for initialization
	void Start () {
		player = FindObjectOfType<FirstPersonController> ();

		textBox.SetActive (false);
		isActive = false;

		if (textFile != null)
			textLines = textFile.text.Split ('\n');

		if (endAtLine == 0)
			endAtLine = textLines.Length - 1;
	}
	
	// Update is called once per frame
	void Update () {
		if (!isActive)
			return;

		if (currentLine <= endAtLine) 
			words = textLines [currentLine].Split (' ');

		if (Input.GetMouseButtonDown (0) && currentLine <= endAtLine && isActive) {
			if (!isTyping) {
				currentLine++;
				if (currentLine > endAtLine)
					DisableTextBox ();
				else
					StartCoroutine (TextScroll (words.First (),String.Join (" ", words.Skip (1).ToArray ())));
				
			} else if (isTyping && !cancelTyping) {
				cancelTyping = true;
			}
		}
	}

	private IEnumerator TextScroll (string name, string speak){
		int letter = 0;
		dialogueText.text = "";
		isTyping = true;
		cancelTyping = false;

		if (words.First () == "System") {
			nameText.color = Color.red;
			dialogueText.color = Color.red;
		} else if (words.First() == "Player" || words.First() == "Gorman") {
			nameText.color = Color.black;
			dialogueText.color = Color.black;
		} else if (words.First() == "???" || words.First() == "February") {
			nameText.color = Color.black;
			dialogueText.color = Color.black;
		}
			
		nameText.text = words.First ();

		while (isTyping && !cancelTyping && (letter < speak.Length - 1)) {
			dialogueText.text += speak [letter];
			letter++;
			yield return new WaitForSeconds (typeSpeed);
		}

		dialogueText.text = speak;
		isTyping = false;
		cancelTyping = false;
		
	}

	public void EnableTextBox(){
		backGround.SetActive (true);
		textBox.SetActive (true);
		isActive = true;
		stopMovement = false;
		player.canMove = false;
		barrier.textBoxOn = true;

		//Time.timeScale = 0f;

		words = textLines [currentLine].Split (' ');
		words.First ();
		StartCoroutine ((TextScroll (words.First (), String.Join (" ", words.Skip (1).ToArray ()))));
		currentLine++;
	}

	public void DisableTextBox(){
		backGround.SetActive (false);
		textBox.SetActive (false);
		isActive = false;
		stopMovement = true;
		player.canMove = true;
		barrier.textBoxOn = false;

		//Time.timeScale = 1f;
	}

	public void ReloadDialogue(TextAsset textFile){
		textLines = new string[1];
		textLines = textFile.text.Split ('\n');
	}
}
