﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivateBarrier : MonoBehaviour {

	public GameObject barrier;
	public ParticleSystem trail;
	public ParticleSystem light;
	public float maxActiveTime;
	public float regenDelay;
	public float regenTime;
	public Slider energyBar;
	public Text currentEnergyText;
	private bool holding = false;
	private bool active = true;
	private bool regenarating = false;
	private float regenTimer = 0f;
	private AudioSource audio;

	public bool textBoxOn;

	// Use this for initialization
	void Start () {
		StopGunParticle ();
		barrier.SetActive (false);
		audio = GetComponent<AudioSource> ();
		currentEnergyText.text = "100";

		textBoxOn = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.timeScale == 0f) {
			return;
		}
		if (Input.GetMouseButton (0) && active && !holding && energyBar.value > 0f && !textBoxOn) {
			regenTimer = 0f;
			audio.Play ();
			holding = true;
			regenarating = false;
			StartGunParticle ();
			barrier.SetActive (true);
		} else if (!Input.GetMouseButton (0) || !active || !holding) {
			audio.Stop ();
			holding = false;
			StopGunParticle ();
			barrier.SetActive (false);
			if (energyBar.value < 1f) {
				regenTimer += Time.deltaTime;
				if (regenTimer >= regenDelay) {
					float currentEnergy = energyBar.value;
					currentEnergy = currentEnergy + (1f / (regenTime / Time.deltaTime));
					energyBar.value = currentEnergy;
					currentEnergyText.text = ((int) (energyBar.value * 100)).ToString();
				}
			}
		}
		if (holding) {
			regenarating = false;
			float currentEnergy = energyBar.value;
			currentEnergy = currentEnergy - (1f / (maxActiveTime / Time.deltaTime));
			energyBar.value = currentEnergy;
			currentEnergyText.text = ((int) (energyBar.value * 100)).ToString();
			if (currentEnergy <= 0f) {
				holding = false;
			} 
		} 
	}

	private void StartGunParticle () {
		trail.Play ();
		light.Play ();
	}

	private void StopGunParticle () {
		trail.Stop ();
		light.Clear ();
		light.Stop ();
	}

	public void SetActive (bool active) {
		this.active = active;
	}
}
