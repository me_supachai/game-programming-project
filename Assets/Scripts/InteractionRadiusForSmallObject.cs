﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionRadiusForSmallObject : MonoBehaviour {

	private ObjectInteraction oi;

	void Start() {
		oi = GameObject.Find ("Object Interaction").gameObject.GetComponent<ObjectInteraction> ();
	}

	void OnTriggerStay(Collider other) {
		if (other.tag == "Player") {
			oi.SetIsInInteractionRadius (true);
		}
	}

	void OnTriggerExit(Collider other) {
		if (other.tag == "Player") {
			oi.SetIsInInteractionRadius (false);
		}
	}
}
