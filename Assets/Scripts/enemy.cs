﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine;
using UnityEngine.AI;

public class enemy : MonoBehaviour {

	public float walkSpeed;
	public float sprintSpeed;
	public float lostSightTime;
	private float lostSightTimer;
	public float distractTime;
	private float distractTimer;

	private GameObject player;
	private FirstPersonController fpc;
	private NavMeshAgent agent ;

	public DialogueManager dialogueManager;

	public Transform[] points;
	private int desPoint;

	private bool chasing = false;
	private bool lostSight = true;
	private bool setDestination = true;
	private bool goOffRoute = false;
	private Vector3 intendedDestination;

	public AudioClip jumpScareSound;
	private AudioSource source;

	// Use this for initialization
	void Awake () {
		source = GetComponent<AudioSource>();
		player = GameObject.FindGameObjectWithTag ("Player");
		fpc = GameObject.FindGameObjectWithTag ("Player").GetComponent<FirstPersonController> ();
		agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
		agent.autoBraking = true;
		agent.updateRotation = true;
		agent.speed = walkSpeed;
		desPoint = 0;
		transform.position = points[desPoint].position;
		intendedDestination = points[desPoint].position;

		dialogueManager = FindObjectOfType<DialogueManager> ();
	}

	// Update is called once per frame
	void Update () {
		if (dialogueManager.isActive) { 
			agent.speed = 0;
			return;
		} else
			agent.speed = walkSpeed;
		
		if (setDestination) {
			setDestination = false;
			agent.destination = intendedDestination;
			return;
		}
		if (chasing) {
			agent.destination = player.transform.position;
			if (lostSight) {
				agent.speed = 1f;
				lostSightTimer += Time.deltaTime;
				if (lostSightTimer >= lostSightTime) {
					chasing = false;
					setDestination = true;
					goOffRoute = false;
					agent.speed = walkSpeed;
					intendedDestination = points [desPoint].position;
				}
			} else {
				agent.speed = sprintSpeed;
			}
			if (Vector3.Distance (transform.position, player.transform.position) < 2f && !fpc.IsDead()) {
				agent.destination = transform.position;
				source.PlayOneShot(jumpScareSound,1);
				fpc.Die(transform.position);
			}
		} else if (agent.remainingDistance < 0.2f) {
			if (!goOffRoute) {
				setDestination = true;
				desPoint = (desPoint + 1) % points.Length;
			} else {
				distractTimer += Time.deltaTime;
				if (distractTimer >= distractTime) {
					goOffRoute = false;
					setDestination = true;
				}
			}
			intendedDestination = points[desPoint].position;
		}
	}

	void GoToNextPoint () {
		if (points.Length == 0) {
			return;
		}

		desPoint = (desPoint + 1) % points.Length;
		agent.destination = points [desPoint].position;

	}

	public void GoOffRoute(Vector3 destination){
		if (lostSight) {
			distractTimer = 0f;
			setDestination = true;
			goOffRoute = true;
			intendedDestination = destination;
		}
	}	

	void OnTriggerStay(Collider other){
		if (other.tag == "Player") {
			RaycastHit hit;
			if (Physics.Raycast (transform.position, other.transform.position - transform.position, out hit, 10000)) {	
				if (hit.collider.CompareTag ("Player")) {
					chasing = true;
					lostSight = false;
					lostSightTimer = 0f;
				} else {
					lostSight = true;
				}
			} 
		}
	}

	void OnTriggerExit(Collider other){
		if (other.tag == "Player") {
			lostSight = true;
		}
	}
}
