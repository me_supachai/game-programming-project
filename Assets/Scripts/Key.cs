﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour {
	private bool taken = false;

	void Update() {
		if (Time.timeScale == 0f) {
			return;
		}
		transform.Rotate (new Vector3 (15, 30, 45) * Time.deltaTime);
	}

	public void PickUp() {
		taken = true;
		GetComponent<Renderer> ().enabled = false;
	}

	public bool IsTaken() {
		return taken;
	}
}
