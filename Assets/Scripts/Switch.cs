﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour {
	public GameObject pointlight;
	private Renderer renderer;

	void Start() {
		renderer = GetComponent<Renderer> ();
		if (pointlight.activeSelf) {
			renderer.material.color = new UnityEngine.Color (0f, 1f, 1f, 1f);
		} else {
			renderer.material.color = new UnityEngine.Color (1f, 0f, 0f, 1f);
		}
	}

	public void ToggleLight() {
		pointlight.SetActive (!pointlight.activeSelf);
		if (pointlight.activeSelf) {
			renderer.material.color = new UnityEngine.Color (0f, 1f, 1f, 1f);
		} else {
			renderer.material.color = new UnityEngine.Color (1f, 0f, 0f, 1f);
		}
	}
}
