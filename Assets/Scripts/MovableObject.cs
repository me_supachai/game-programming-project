﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class MovableObject : MonoBehaviour {

	public GameObject largeTriggerPoint;
	public GameObject smallTriggerPoint;
	private GameObject player;
	private GameObject cam;
	private Rigidbody rigidbody;
	private BoxCollider collider;
	private Renderer renderer;
	private GameObject border;
	private bool moved = false;
	private bool held = false;
	private bool thrown = false;
	private GameObject ohp; 
	private GameObject oi;
	private Transform rsp;

	private bool contactWall = false;
	private Vector3 contactPosition;
	private Vector3 playerContactPosition;

	private float minimumY;
	private bool spawnHit = true;
	private bool triggerSelector = false;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		cam = GameObject.FindGameObjectWithTag ("MainCamera");
		collider = GetComponent<BoxCollider> ();
		rigidbody = GetComponent<Rigidbody> ();
		renderer = GetComponent<Renderer> ();
		oi = GameObject.Find ("Object Interaction");
		if (gameObject.tag == "Movable Object") {
			ohp = GameObject.Find ("Object Holding Position");
		} else {
			ohp = GameObject.Find ("Object Interaction");
		}
		rsp = GameObject.Find ("Raycast Start Position").gameObject.transform;
		StartCoroutine (WaitToGetMinimumY());
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (moved) {
			rigidbody.useGravity = false;
			float pullSpeed = 0f;
			Vector3 dir = (ohp.transform.position - transform.position).normalized;
			if (Vector3.Distance (transform.position, ohp.transform.position) >= 1f) {
				pullSpeed = 16f;
			} else if (Vector3.Distance (transform.position, ohp.transform.position) >= 0.5f) {
				pullSpeed = 8f;
			} else if (Vector3.Distance (transform.position, ohp.transform.position) >= 0.25f) {
				pullSpeed = 4f;
			} else if (Vector3.Distance (transform.position, ohp.transform.position) >= 0.1f) {
				pullSpeed = 2f;
			} else if (Vector3.Distance (transform.position, ohp.transform.position) >= 0.05f) {
				pullSpeed = 1f;
			}
			rigidbody.MovePosition (transform.position + dir * pullSpeed * Time.deltaTime);
			if (Vector3.Distance (transform.position, ohp.transform.position) < 0.1f) {
				moved = false;
				held = true;
			}
		} else if (held) {
			Vector3 oiPosition = ohp.transform.position;
			if (oiPosition.y < minimumY) {
				oiPosition.y = minimumY;
			} 
			transform.position = oiPosition;
			Vector3 dir = rsp.position - transform.position;
			dir.y = 0f;
			transform.rotation = Quaternion.LookRotation (dir);
		} else if (thrown) {
			rigidbody.AddForce ((oi.transform.position - cam.transform.position).normalized * 900f);
			thrown = false;
		}
	}

	public void MoveToPlayer(Vector3 destination) {
		if (gameObject.tag == "Movable Object") {
			renderer.material.color = new UnityEngine.Color (1f, 1f, 1f, 0.7f);
		}
		gameObject.layer = 2;
		renderer.shadowCastingMode = ShadowCastingMode.Off;
		renderer.receiveShadows = false;
		contactWall = false;
		moved = true;
		held = false;
		thrown = false;
		collider.isTrigger = true;
		rigidbody.useGravity = false;
		rigidbody.isKinematic = true;
	}

	public void Release() {
		if (gameObject.tag == "Movable Object") {
			renderer.material.color = new UnityEngine.Color (1f, 1f, 1f, 1f);
		}
		gameObject.layer = 0;
		renderer.shadowCastingMode = ShadowCastingMode.On;
		renderer.receiveShadows = true;
		if (contactWall) {
			player.transform.position = playerContactPosition;
			transform.position = contactPosition;
			StartCoroutine (WaitToActivatePhysics());
			return;
		}
		triggerSelector = false;
		contactWall = false;
		moved = false;
		held = false;
		thrown = false;
		collider.isTrigger = false;
		rigidbody.useGravity = true;
		rigidbody.isKinematic = false;
	}

	public void Throw() {
		if (gameObject.tag == "Movable Object") {
			renderer.material.color = new UnityEngine.Color (1f, 1f, 1f, 1f);
		}
		gameObject.layer = 0;
		renderer.shadowCastingMode = ShadowCastingMode.On;
		renderer.receiveShadows = true;
		if (contactWall) {
			player.transform.position = playerContactPosition;
			transform.position = contactPosition;
			StartCoroutine (WaitToActivatePhysics());
			return;
		}
		triggerSelector = true;
		contactWall = false;
		moved = false;
		held = false;
		thrown = true;
		collider.isTrigger = false;
		rigidbody.useGravity = true;
		rigidbody.isKinematic = false;
	}

	public bool IsBeingMoved() {
		return moved;
	}

	public bool IsBeingHeld() {
		return held;
	}

	private IEnumerator WaitToActivatePhysics() {
		yield return new WaitForSeconds (0.1f);
		contactWall = false;
		moved = false;
		held = false;
		thrown = false;
		collider.isTrigger = false;
		rigidbody.useGravity = true;
		rigidbody.isKinematic = false;
	}

	private IEnumerator WaitToGetMinimumY() {
		yield return new WaitForSeconds (1f);
		minimumY = transform.position.y;
	}

	void OnTriggerEnter(Collider other) {
		if ((other.tag == "Wall" || other.tag == "Movable Object") && !contactWall) {
			contactWall = true;
			contactPosition = transform.position;
			playerContactPosition = player.transform.position;
		}
	}

	void OnTriggerExit(Collider other) {
		if (other.tag == "Wall" || other.tag == "Movable Object") {
			contactWall = false;
		}
	}

	void OnCollisionEnter(Collision other) {
		if (other.collider.tag == "Floor") {
			if (spawnHit) {
				spawnHit = false;
			} else {
				if (!triggerSelector) {
					Instantiate (smallTriggerPoint, transform.position, Quaternion.identity);
				} else {
					Instantiate (largeTriggerPoint, transform.position, Quaternion.identity);
				}
			}
		}
	}
}
