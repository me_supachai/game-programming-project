﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunPullUpTrigger : MonoBehaviour {

	private GameObject gun;
	private GameObject rotationCenter;
	private bool pullUp = false;
	private bool pullDown = false;

	private float pullUpAngle = 0f;

	// Use this for initialization
	void Start () {
		gun = GameObject.Find ("Gun");
		rotationCenter = GameObject.Find ("Rotation Center");
	}

	void OnTriggerEnter(Collider other) {
		if (gun != null) {
			if ((other.tag == "Wall" || other.tag == "Movable Object" || other.tag == "Door" || other.tag == "Door RK") && !pullUp) {
				pullUp = true;
				pullDown = false;
				gun.GetComponent<ActivateBarrier> ().SetActive (false);
			}
		}
	} 

	void OnTriggerExit(Collider other) {
		if (gun != null) {
			if (other.tag == "Wall" || other.tag == "Movable Object" || other.tag == "Door" || other.tag == "Door RK") {
				pullUp = false;
				pullDown = true;
			}
		}
	}

	// Update is called once per frame
	void Update () {
		if (pullUp) {
			if (pullUpAngle < 50f) {
				pullUpAngle += Time.deltaTime * 250f;
				gun.transform.RotateAround (rotationCenter.transform.position, rotationCenter.transform.right * -1f, Time.deltaTime * 250f);
			} else if (pullUpAngle < 59f) {
				pullUpAngle += Time.deltaTime * 100f;
				gun.transform.RotateAround (rotationCenter.transform.position, rotationCenter.transform.right * -1f, Time.deltaTime * 100f);
			} else if (pullUpAngle < 61.5f) {
				pullUpAngle += Time.deltaTime * 50f;
				gun.transform.RotateAround (rotationCenter.transform.position, rotationCenter.transform.right * -1f, Time.deltaTime * 50f);
			} else if (pullUpAngle < 65f) {
				pullUpAngle += Time.deltaTime * 10f;
				gun.transform.RotateAround (rotationCenter.transform.position, rotationCenter.transform.right * -1f, Time.deltaTime * 10f);
			}
			if (pullUpAngle > 65f) {
				pullUp = false;
			}
		} else if (pullDown) {
			if (pullUpAngle > 15f) {
				pullUpAngle -= Time.deltaTime * 250f;
				gun.transform.RotateAround (rotationCenter.transform.position, rotationCenter.transform.right, Time.deltaTime * 250f);
			} else if (pullUpAngle > 6f) {
				pullUpAngle -= Time.deltaTime * 100f;
				gun.transform.RotateAround (rotationCenter.transform.position, rotationCenter.transform.right, Time.deltaTime * 100f);
			} else if (pullUpAngle > 3.5f) {
				pullUpAngle -= Time.deltaTime * 50f;
				gun.transform.RotateAround (rotationCenter.transform.position, rotationCenter.transform.right, Time.deltaTime * 50f);
			} else if (pullUpAngle > 0f) {
				pullUpAngle -= Time.deltaTime * 10f;
				gun.transform.RotateAround (rotationCenter.transform.position, rotationCenter.transform.right, Time.deltaTime * 10f);
			}
			if (pullUpAngle < 0f) {
				if (pullUpAngle < -3.5f) {
					pullUpAngle += Time.deltaTime * 50f;
					gun.transform.RotateAround (rotationCenter.transform.position, rotationCenter.transform.right * -1f, Time.deltaTime * 50f);
				} else if (pullUpAngle < -0.6f) {
					pullUpAngle += Time.deltaTime * 10f;
					gun.transform.RotateAround (rotationCenter.transform.position, rotationCenter.transform.right * -1f, Time.deltaTime * 10f);
				} else if (pullUpAngle < -0.2f) {
					pullUpAngle += Time.deltaTime * 2f;
					gun.transform.RotateAround (rotationCenter.transform.position, rotationCenter.transform.right * -1f, Time.deltaTime * 2f);
				} else {
					pullDown = false;
					gun.GetComponent<ActivateBarrier> ().SetActive (true);
				}
			}
		}
	}
}
