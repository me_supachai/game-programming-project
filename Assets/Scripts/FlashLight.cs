﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLight : MonoBehaviour {
	
	private Light light;

	// Use this for initialization
	void Start () {
		light = GetComponent<Light> ();
		light.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.timeScale == 0f) {
			return;
		}
		if (Input.GetKeyDown (KeyCode.X)) {
			light.enabled = !light.enabled;
		}
	}
}
