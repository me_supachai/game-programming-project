﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyBarShadowColor : MonoBehaviour {

	private Shadow shadow;
	private Slider slider;

	// Use this for initialization
	void Start () {
		shadow = GetComponent<Shadow> ();
		slider = transform.parent.gameObject.GetComponent<Slider> ();
	}
	
	// Update is called once per frame
	void Update () {
		shadow.effectColor = new Color (1f - slider.value, 0f, 0f, 0.8f);
	}
}
