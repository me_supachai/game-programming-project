﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

	public Canvas pauseCanvas;
	public Canvas deathCanvas;
	public Canvas clearCanvas;

	public DialogueManager dialogueManager;

	private FirstPersonController fpc;

	private bool stageClear;

	void Start () {
		Time.timeScale = 1f;
		stageClear = false;
		pauseCanvas.enabled = false;
		deathCanvas.enabled = false;
		clearCanvas.enabled = false;
		fpc = GameObject.FindGameObjectWithTag ("Player").GetComponent<FirstPersonController> ();

		dialogueManager = FindObjectOfType<DialogueManager> ();
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape) && !fpc.IsDead () && !stageClear) {
			if (Time.timeScale == 0f) {
				Time.timeScale = 1f;
			} else {
				if (!dialogueManager.isActive)
					Time.timeScale = 0f;
			}
		}
		if (fpc.CanDisplayDeath()) {
			Time.timeScale = 0f;
			Cursor.visible = true;
			Cursor.lockState = CursorLockMode.None;
			deathCanvas.enabled = true;
			return;
		}
		if (stageClear) {
			Time.timeScale = 0f;
			Cursor.visible = true;
			Cursor.lockState = CursorLockMode.None;
			clearCanvas.enabled = true;
			return;
		}
		if (Time.timeScale == 0f && !dialogueManager.isActive) {
			Cursor.visible = true;
			Cursor.lockState = CursorLockMode.None;
			pauseCanvas.enabled = true;
		} else {
			Cursor.visible = false;
			Cursor.lockState = CursorLockMode.Locked;
			pauseCanvas.enabled = false;
		} 	
	}

	public void RestartScene() {
		Application.LoadLevel (Application.loadedLevel);
	}

	public void Resume() {
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;
		Time.timeScale = 1f;
		pauseCanvas.enabled = false;
	}

	public void BackToMenu() {
		SceneManager.LoadScene (0, LoadSceneMode.Single);
	}

	public void LoadNextScene() {
		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex + 1, LoadSceneMode.Single);
	}

	public void Clear() {
		stageClear = true;
	}
}
