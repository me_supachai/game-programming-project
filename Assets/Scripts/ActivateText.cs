﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateText : MonoBehaviour {

	public int startLine;
	public int endLine;

	public TextAsset TextFile;

	public DialogueManager theTextBox;

	public bool requireButtonPress;
	private bool waitForPress;

	public bool destroyWhenActivated;

	// Use this for initialization
	void Start () {
		theTextBox = FindObjectOfType<DialogueManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (waitForPress && Input.GetMouseButtonDown (1)) {
			theTextBox.ReloadDialogue (TextFile);
			theTextBox.currentLine = startLine;
			theTextBox.endAtLine = endLine;
			theTextBox.EnableTextBox ();

			if (destroyWhenActivated)
				Destroy (gameObject);
		}
	}

	void OnTriggerEnter(Collider other){
		if (other.name == "FPSController") {
			if (requireButtonPress) {
				waitForPress = true;
				return;
			}

			theTextBox.ReloadDialogue (TextFile);
			theTextBox.currentLine = startLine;
			theTextBox.endAtLine = endLine;
			theTextBox.EnableTextBox ();

			if (destroyWhenActivated)
				Destroy (gameObject);
		}
	}

	void OnTriggerExit(Collider other){
		if (other.name == "FPSController") 
			waitForPress = false;
	}
}
