﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour {

	public Canvas mainCanvas;
	public Canvas stagesCanvas;
	public float fadeSpeed;

	private bool selectStages = false;
	private bool selectBack = false;

	// Use this for initialization
	void Start () {
		selectStages = false;
		selectBack = false;
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
		mainCanvas.enabled = true;
		mainCanvas.GetComponent<CanvasGroup> ().blocksRaycasts = true;
		mainCanvas.GetComponent<CanvasGroup> ().alpha = 1f;
		stagesCanvas.enabled = false;
		stagesCanvas.GetComponent<CanvasGroup> ().blocksRaycasts = false;
		stagesCanvas.GetComponent<CanvasGroup> ().alpha = 0f;
	}

	// Update is called once per frame
	void Update () {
		if (selectStages) {
			if (mainCanvas.enabled) {
				mainCanvas.GetComponent<CanvasGroup> ().alpha -= fadeSpeed;
				if (mainCanvas.GetComponent<CanvasGroup> ().alpha <= 0f) {
					mainCanvas.GetComponent<CanvasGroup> ().alpha = 0f;
					mainCanvas.enabled = false;
					stagesCanvas.enabled = true;
				}
			} else {
				stagesCanvas.GetComponent<CanvasGroup> ().alpha += fadeSpeed;
				if (stagesCanvas.GetComponent<CanvasGroup> ().alpha >= 1f) {
					stagesCanvas.GetComponent<CanvasGroup> ().alpha = 1f;
					stagesCanvas.GetComponent<CanvasGroup> ().blocksRaycasts = true;
					selectStages = false;
				}
			}
		} else if (selectBack) {
			if (stagesCanvas.enabled) {
				stagesCanvas.GetComponent<CanvasGroup> ().alpha -= fadeSpeed;
				if (stagesCanvas.GetComponent<CanvasGroup> ().alpha <= 0f) {
					stagesCanvas.GetComponent<CanvasGroup> ().alpha = 0f;
					stagesCanvas.enabled = false;
					mainCanvas.enabled = true;
				}
			} else {
				mainCanvas.GetComponent<CanvasGroup> ().alpha += fadeSpeed;
				if (mainCanvas.GetComponent<CanvasGroup> ().alpha >= 1f) {
					mainCanvas.GetComponent<CanvasGroup> ().alpha = 1f;
					mainCanvas.GetComponent<CanvasGroup> ().blocksRaycasts = true;
					selectBack = false;
				}
			}
		}
	}

	public void ClickSelectStages() {
		selectStages = true;
		mainCanvas.GetComponent<CanvasGroup> ().blocksRaycasts = false;
	}

	public void ClickBack() {
		selectBack = true;
		stagesCanvas.GetComponent<CanvasGroup> ().blocksRaycasts = false;
	}

	public void ClickExit() {
		Application.Quit ();
	}

	public void ClickStage(int stage) {
		SceneManager.LoadScene (stage, LoadSceneMode.Single);
	} 
}
