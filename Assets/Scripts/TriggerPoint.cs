﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPoint : MonoBehaviour {

	public int age;
	private GameObject[] c;
	private bool alreadyDistract = false;

	// Use this for initialization
	void Awake () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (age > 0) {
			age--;
		} else {
			Destroy (this.gameObject);
		}
	}

	void OnTriggerEnter(Collider other){
		if (other.tag == "Clone" && !alreadyDistract) {
			alreadyDistract = true;
			c = GameObject.FindGameObjectsWithTag ("Clone");
			Transform[] trans = new Transform[c.Length];
			for(int i = 0; i < c.Length;i++){
				trans [i] = c [i].transform;
			}
				
			enemy e = c[GetClosestEnemy(trans)].gameObject.GetComponent<enemy>();
			e.GoOffRoute (transform.position);
			//Destroy (this.gameObject, 1.5f);
		}
	}

	int GetClosestEnemy(Transform[] enemies){
		int Min = 0;
		float minDist = Mathf.Infinity;
		Vector3 currentPos = transform.position;
		for (int i = 0; i < enemies.Length; i++){
			float dist = Vector3.Distance(enemies[i].position, currentPos);
			if (dist < minDist)
			{
				Min = i;
				minDist = dist;
			}
		}
		return Min;
	}
}
